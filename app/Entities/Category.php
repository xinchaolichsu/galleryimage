<?php

namespace App\Entities;

use App\Entities\Image;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['id', 'name', 'slug'];

    public function users() {
    	return $this->belongsToMany(User::class);
    }

    public function images() {
		return $this->hasMany(Image::class);
	}
}

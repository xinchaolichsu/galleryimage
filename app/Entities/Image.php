<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
		'id', 'user_id', 'link', 'description', 'created_at'
	];

	public function user() {
		$this->belongsTo(User::class);
	}

	public function cate() {
		return $this->belongsTo(Category::class);
	}
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Payment extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
    	'id', 'user_id', 'payment_image', 'account_id', 'created_at', 'amount'
    ];

    public function users() {
		$this->belongsTo(User::class);
	}
}

<?php

namespace App\Repositories;

use App\Entities\Payment;
use App\Repositories\PaymentRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PaymentRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PaymentRepositoryEloquent extends BaseRepository implements PaymentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Payment::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Hàm xây dựng url chuyển đến BaoKim.vn thực hiện thanh toán, trong đó có tham số mã hóa (còn gọi là public key)
     * @param $order_id             Mã đơn hàng
     * @param $business             Email tài khoản người bán
     * @param $total_amount         Giá trị đơn hàng
     * @param $shipping_fee         Phí vận chuyển
     * @param $tax_fee              Thuế
     * @param $order_description    Mô tả đơn hàng
     * @param $url_success          Url trả về khi thanh toán thành công
     * @param $url_cancel           Url trả về khi hủy thanh toán
     * @param $url_detail           Url chi tiết đơn hàng
     * @return url cần tạo
     */
    public function createRequestUrl($params)
    {
        $merchant_id = getenv('MERCHANT_ID');
        $secure_pass = getenv('SECURE_PASS');
        $baokim_url  = getenv('BAOKIM_URL');
        // Mảng các tham số chuyển tới baokim.vn
        $params['merchant_id'] = strval($merchant_id);

        ksort($params);

        $str_combined       = $secure_pass . implode('', $params);
        $params['checksum'] = strtoupper(md5($str_combined));

        //Kiểm tra  biến $redirect_url xem có '?' không, nếu không có thì bổ sung vào
        $redirect_url = $baokim_url;
        if (strpos($redirect_url, '?') === false) {
            $redirect_url .= '?';
        } else if (substr($redirect_url, strlen($redirect_url) - 1, 1) != '?' && strpos($redirect_url, '&') === false) {
            // Nếu biến $redirect_url có '?' nhưng không kết thúc bằng '?' và có chứa dấu '&' thì bổ sung vào cuối
            $redirect_url .= '&';
        }

        // Tạo đoạn url chứa tham số
        $url_params = '';
        foreach ($params as $key => $value) {
            if ($url_params == '') {
                $url_params .= $key . '=' . urlencode($value);
            } else {
                $url_params .= '&' . $key . '=' . urlencode($value);
            }

        }
        return $redirect_url . $url_params;
    }

    /**
     * Hàm thực hiện xác minh tính chính xác thông tin trả về từ BaoKim.vn
     * @param $_GET chứa tham số trả về trên url
     * @return true nếu thông tin là chính xác, false nếu thông tin không chính xác
     */
    public function verifyResponseUrl($request_all, $checksum)
    {
        // $checksum = $request->get('checksum');
        // unset($request->get('checksum'));

        ksort($request_all);
        $str_combined = getenv('SECURE_PASS') . implode('', $request_all);

        // Mã hóa các tham số
        $verify_checksum = strtoupper(md5($str_combined));

        // Xác thực mã của chủ web với mã trả về từ baokim.vn
        if ($verify_checksum === $checksum) {
            return true;
        }

        return false;
    }
}

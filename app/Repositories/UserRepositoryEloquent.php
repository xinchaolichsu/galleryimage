<?php

namespace App\Repositories;

use App\Entities\Category;
use App\Entities\Image;
use App\Entities\User;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use NF\Roles\Models\Role;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return User::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	public function get_list() {
		$curr_user = Auth::user();
		$users = User::where('id', "<>", $curr_user->id)->orderBy('id', 'desc')->get();
		if (empty($users)) {
			return;
		}
		$users = $users->map(function ($item) {
			$item->role = $item->level();
			$item->payment = 0;
			if ($item->payment == 2) {
				$item->payment = "<p class=' alert-success'><b>Đã thanh toán</b></p>";
			} elseif ($item->payment == 1) {
				$item->payment = "<p class=' alert-warning'><b>Đang chờ</b></p>";
			} else {
				$item->payment = "<p class=' alert-danger'><b>Chưa thanh toán</b></p>";
			}

			if ($item->isRegister == 1) {
				$item->isRegister = "<p class=' alert-success'><b>Đã đăng ký</b></p>";
			} else {
				$item->isRegister = "<p class=' alert-danger'><b>Tự do</b></p>";
			}

			$image = Image::where('user_id', $item->id)->first();
			if (!empty($image)) {
				$check = $image['approve'];
			} else {
				$check = 0;
			}
			if ($check == 2) {
				$item->approve = "<p class=' alert-danger'>Không phê duyệt</p>";
			} elseif ($check == 1) {
				$item->approve = "<p class=' alert-success'>Đã phê duyệt</p>";
			} else {
				$item->approve = "<p class=' alert-warning'>Chưa phê duyệt</p>";
			}
			$item->created_at = Carbon::parse($item->created_at)->format('Y-m-d');
			return $item;
		});

		$result = [];

		if ($curr_user->isRole('admin')) {
			foreach ($users as $key => $user) {
				$result[] = $user;
			}
		} elseif ($curr_user->isRole('editor')) {
			foreach ($users as $key => $user) {
				if (!$user->isRole('admin')) {
					$result[] = $user;
				}
			}
		} elseif ($curr_user->isRole('checker')) {
			foreach ($users as $key => $user) {
				if (!$user->isRole('admin') && !$user->isRole('editor')) {
					$result[] = $user;
				}
			}
		} else {
			$result = User::where('id', $curr_user->id)->get();
			$result = $result->map(function ($item) {
				$item->role = $item->level();
				$item->payment = 0;
				if ($item->payment == 2) {
					$item->payment = "<p class=' alert-success'><b>Đã thanh toán</b></p>";
				} elseif ($item->payment == 1) {
					$item->payment = "<p class=' alert-warning'><b>Đang chờ</b></p>";
				} else {
					$item->payment = "<p class=' alert-danger'><b>Chưa thanh toán</b></p>";
				}

				if ($item->isRegister == 1) {
					$item->isRegister = "<p class=' alert-success'><b>Đã đăng ký</b></p>";
				} else {
					$item->isRegister = "<p class=' alert-danger'><b>Tự do</b></p>";
				}

				$image = Image::where('user_id', $item->id)->first();
				if (!empty($image)) {
					$check = $image['approve'];
				} else {
					$check = 0;
				}
				if ($check == 2) {
					$item->approve = "<p class=' alert-danger'>Không phê duyệt</p>";
				} elseif ($check == 1) {
					$item->approve = "<p class=' alert-success'>Đã phê duyệt</p>";
				} else {
					$item->approve = "<p class=' alert-warning'>Chưa phê duyệt</p>";
				}
				$item->created_at = Carbon::parse($item->created_at)->format('Y-m-d');
				return $item;
			});
		}
		$result = new Collection($result);
		return $result;
	}

	public function get_once($id) {
		if (is_numeric($id)) {
			$user = User::where('id', $id)->get();
			$user = $user->map(function ($item) {
				$tmp = [];
				$total = [];
				$image = Image::where('user_id', $item->id)->get();
				if (!empty($image)) {
					foreach ($image as $key_image => $img) {
						$link = json_decode($img['link']);
						if (!empty($link)) {
							foreach ($link->url_image as $key => $url) {
								$exp = explode('/', $url);
								$tmp[$key]['url'] = $url;
								$tmp[$key]['download'] = last($exp);
							}
							$total[$key_image]['link'] = $tmp;
						} else {
							$total[$key_image]['link'] = '';
						}
						$total[$key_image]['id_image'] = $img['id'];
						$cate = Category::find($img['cate_id']);
						if (!empty($cate)) {
							$total[$key_image]['cate_name'] = $cate['name'];
							$total[$key_image]['cate_id'] = $cate['id'];
						} else {
							$total[$key_image]['cate_name'] = '';
							$total[$key_image]['cate_id'] = '';
						}
						$total[$key_image]['description'] = $img['description'];
						$total[$key_image]['created_at'] = $img['created_at'];
						$total[$key_image]['approve'] = $img['approve'];
					}
					$item->image = $total;
				} else {
					$item->image = '';
				}
				return $item;
			});
			return $user[0];
		}
	}

	public function delete_once($id) {
		$user = User::find($id);
		$user->delete();
		return $user;
	}
}

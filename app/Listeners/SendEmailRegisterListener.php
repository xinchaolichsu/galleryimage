<?php

namespace App\Listeners;

use App\Events\SendMailRegister;
use App\Mail\EmailRegister;
use Illuminate\Support\Facades\Mail;

class SendEmailRegisterListener {
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  SomeEvent  $event
	 * @return void
	 */
	public function handle(SendMailRegister $event) {
		Mail::to($event->email)
			->queue(new EmailRegister($event->email, $event->password));
	}
}

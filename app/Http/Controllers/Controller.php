<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function download_file($file_path, $file_name)
    {
        if (file_exists($file_path)) {
            return Response::download($file_path, $file_name, [
                'Content-Length: ' . filesize($file_path),
            ]);
        } else {
            return false;
        }
    }
}

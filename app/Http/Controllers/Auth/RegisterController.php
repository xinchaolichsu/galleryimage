<?php

namespace App\Http\Controllers\Auth;

use App\Entities\Image;
use App\Entities\Role;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'isRegister' => $data['isRegister']
        ]);
    }

    public function register(Request $request) {
        $user = new User();
        $user->name = ($request->has('name') ? $request->get('name') : '');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->isRegister = 1;
        $user->account_id = md5($request->get('email'));
        $user->save();
        $userRole    = Role::find(4);
        $user->attachRole($userRole);
        $image = new Image;
        $image->user_id = $user->id;
        $image->link = '';
        $image->cate_id = 7;
        $image->description = 'default';
        $image->approve = 0;
        $image->save();
        Auth::login($user);
        return redirect('/admin')->with(['code' => 'success', 'message' => 'Đăng ký thành công']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Entities\Image;
use App\Entities\User;
use App\Events\SendMailRegister;
use App\Http\Requests\HomepageRequest;
use App\Repositories\PaymentRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;

class HomepageController extends Controller {
	protected $payment;
	public function __construct(PaymentRepository $payment) {
		$this->payment = $payment;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$params = [
			'order_id' => strval(1),
			'business' => strval('xinchaolichsu@gmail.com'),
			'total_amount' => strval(10000),
			'shipping_fee' => strval(0),
			'tax_fee' => strval(0),
			'order_description' => strval('Thanh toán cho lưu trữ ảnh'),
			'url_success' => strtolower(url('/payment/callback')),
			'url_cancel' => strtolower(url('/')),
			'url_detail' => strtolower(''),
		];
		$btn_url = $this->payment->createRequestUrl($params);
		$categories = Category::all();
		return view('home.index', compact('btn_url', 'categories'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(HomepageRequest $request) {
		if ($request->has('cate-image') && ($request->get('cate-image') == 0)) {
			return redirect()->back()->with(['code' => 'danger', 'status' => 'Chọn danh mục cho ảnh trước khi hoàn tất!']);
		}
		if (!$request->has('form-data-u')) {
			return redirect()->back()->with(['code' => 'danger', 'message' => 'Không tìm thấy người dùng!']);
		}
		if (!$request->has('check_image')) {
			return redirect()->back()->with(['code' => 'danger', 'status' => 'Không tìm thấy ảnh lưu trữ!']);
		}

		if (!empty(Auth::user())) {
			$user = Auth::user();
		} else {
			$user = User::find($request->get('form-data-u'));
			if (empty($user)) {
				return redirect()->back()->with(['code' => 'danger', 'status' => 'Không tìm thấy người dùng!']);
			}
			if (!empty(Auth::user())) {
				$user->email = Auth::user()->email;
			} else {
				if ($request->has('email')) {
					$user->email = $request->get('email');
				} else {
					return redirect()->back()->with(['code' => 'danger', 'status' => 'Không tìm thấy email!']);
				}
			}
			$user->isSubmit = 1;
			$user->isRegister = 1;
			$de_password = str_random(10);
			$user->password = Hash::make($de_password);
			$user->save();
		}

		$image = Image::find($request->get('check_image'));
		if (empty($image)) {
			return redirect()->back()->with(['code' => 'danger', 'message', 'Không tìm thấy ảnh lưu trữ!']);
		}
		$image->description = (!empty($request->get('requirement')) ? $request->get('requirement') : '');
		if ($request->has('cate-image')) {
			$image->cate_id = $request->get('cate-image');
		}
		$image->user_id = $user->id;
		$image->save();
		if (!empty($de_password)) {
			event(new SendMailRegister($request->get('email'), $de_password));
			return redirect()->back()->with(['code' => 'success', 'message' => 'Hoàn tất! Chúng tôi đã đăng ký tài khoản giúp bạn. Hãy kiểm tra email của bạn !']);
		} else {
			return redirect()->back()->with(['code' => 'success', 'message' => 'Hoàn tất! Ảnh của bạn đã được lưu trữ !']);
		}
	}

	public function upload_ajax(Request $request) {
		$image_json['url_image'] = [];
		$image_json['user_id'] = '';

		if ($request->hasFile('file')) {
			$user = User::create([
				'email' => 'tmp@gmail.com',
				'isRegister' => 0,
			]);
			$image_json['user_id'] = $user->id;
			$files = $request->file('file');
			$dt = Carbon::parse(Carbon::today());
			$year = $dt->year;
			$month = $dt->month;
			$day = $dt->day;
			$hour = $dt->hour;
			$timestamp = $dt->timestamp;
			$path = "/assets/uploads";
			if (!file_exists(public_path() . $path)) {
				File::makeDirectory(public_path() . $path);
			}
			foreach ($files as $key => $file) {
				$mimeType = $file->getClientMimeType();
				$mime = explode('/', $mimeType);
				$name_path = $timestamp . "-" . $year . $month . $day . '-' . (time()) . '-' . (str_slug($file->getClientOriginalName())) . "." . $mime[1];
				$file->move(public_path() . $path, $name_path);
				$image_json['url_image'][] = $path . "/" . $name_path;
			}
			$tmp_image_json = json_encode($image_json);
			$image_db = new Image;
			$image_db->user_id = $user->id;
			$image_db->link = $tmp_image_json;
			$image_db->description = '';
			$image_db->save();

			$image_json['image_id'] = $image_db->id;
			$image_json = json_encode($image_json);
		}
		return $image_json;
	}

	public function checkemail(Request $request) {
		$email = $request->get('email');
		if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
			$check = User::where('email', $email)->first();
			if (!empty($check)) {
				$result['code'] = 1;
				$result['message'] = 'Email đã tồn tại, bạn cần đăng nhập để tiếp tục';
			} else {
				$result['code'] = 2;
				$result['message'] = 'Bạn có thể tiếp tục';
			}
		} else {
			$result['code'] = 0;
			$result['message'] = 'Email không đúng định dạng';
		}
		return json_encode($result);
	}

	public function napthe_ajax(Request $request) {
		header('Content-Type: text/html; charset=utf-8');
		$CORE_API_HTTP_USR = getenv('CORE_API_HTTP_USR');
		$CORE_API_HTTP_PWD = getenv('CORE_API_HTTP_PWD');

		$bk = 'https://www.baokim.vn/the-cao/restFul/send';
		$seri = $request->get('seri_field');
		$sopin = $request->get('pin_field');
		//Loai the cao (VINA, MOBI, VIETEL, VTC, GATE)
		$mang = $request->get('card_id');

		if ($mang == 'MOBI') {
			$ten = "Mobifone";
		} else if ($mang == 'VIETEL') {
			$ten = "Viettel";
		} else if ($mang == 'GATE') {
			$ten = "Gate";
		} else if ($mang == 'VTC') {
			$ten = "VTC";
		} else {
			$ten = "Vinaphone";
		}

		$merchant_id = getenv('MERCHANT_ID');

		$api_username = getenv('API_USERNAME');

		$api_password = getenv('API_PASSWORD');

		$transaction_id = time();

		$secure_code = getenv('SECURE_PASS');

		$arrayPost = [
			'merchant_id' => $merchant_id,
			'api_username' => $api_username,
			'api_password' => $api_password,
			'transaction_id' => $transaction_id,
			'card_id' => $mang,
			'pin_field' => $sopin,
			'seri_field' => $seri,
			'algo_mode' => 'hmac',
		];

		ksort($arrayPost);

		$data_sign = hash_hmac('SHA1', implode('', $arrayPost), $secure_code);

		$arrayPost['data_sign'] = $data_sign;

		$curl = curl_init($bk);

		curl_setopt_array($curl, [
			CURLOPT_POST => true,
			CURLOPT_HEADER => false,
			CURLINFO_HEADER_OUT => true,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTPAUTH => CURLAUTH_DIGEST | CURLAUTH_BASIC,
			CURLOPT_USERPWD => $CORE_API_HTTP_USR . ':' . $CORE_API_HTTP_PWD,
			CURLOPT_POSTFIELDS => http_build_query($arrayPost),
		]);

		$data = curl_exec($curl);
		if (!$data) {
			return false;
		}
		$result = json_decode($data, true);

		$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		if ($status != 200) {
			if ($status != 406) {
				$response['message'] = utf8_decode($result['errorMessage']);
			} else {
				$response['message'] = ($result['errorMessage']);
			}
			$response['amount'] = 0;
		} else {
			$response['amount'] = $result['amount'];
			$response['message'] = '';
		}
		$response['transaction_id'] = $result['transaction_id'];
		$response['status'] = $status;
		$response = json_encode($response);
		return $response;
	}
}

<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Image;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Notifications\SendemailEdituser;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminController extends Controller {
	protected $repository;
	public function __construct(UserRepository $repository) {
		$this->repository = $repository;
		$this->middleware('auth', ['except' => ['sendemail_ajax']]);
	}

	public function index() {
		$users = $this->repository->get_list();
		return view('admin.image.index', compact(['users']));
	}

	public function edit(Request $request, $id) {
		$user = $this->repository->get_once($id);
		return view('admin.image.edit', compact(['user']));
	}

	public function update(Request $request, $id) {
		$user = User::find($id);
		if (empty($user)) {
			return redirect()->back()->withErrors(['code' => 'danger', 'message' => 'Không tìm thấy người dùng này']);
		}
		$image_json['url_image'] = [];
		$image_json['user_id'] = $id;
		if (($request->has('link')) && !empty($request->get('link'))) {
			foreach ($request->get('link') as $key => $link) {
				if (!empty($link)) {
					$image_json['url_image'][] = $link;
				}
			}
		}
		$image_json = json_encode($image_json);
		$image = Image::where('user_id', $id)->get();
		$image[0]->link = $image_json;
		$image[0]->save();
		if (!isset($image[0]->id)) {
			return redirect()->back()->withErrors(['code' => 'danger', 'message' => 'Cập nhật không thành công']);
		}
		return redirect()->back()->withErrors(['code' => 'success', 'message' => 'Cập nhật thành công']);
	}

	public function download(Request $request, $name) {
		$path = public_path() . "/assets/uploads/" . $name;
		$res = $this->download_file($path, $name);
		if ($res) {
			return $res;
		} else {
			throw new Exception("Không thể tải xuống", 1);
		}
	}

	public function sendemail_ajax(Request $request) {
		$user = User::where('email', $request->get('email'))->get();
		$user = $user[0];
		$user->notify(new SendemailEdituser($request->get('email'), $request->get('content')));
		return 1;
	}

	public function change_image_ajax(Request $request, $id) {
		$image_json = '';
		if ($request->hasFile('file')) {
			$user = User::where('id', $id)->get();
			$user = $user[0];
			$file = $request->file('file');
			$dt = Carbon::parse(Carbon::today());
			$year = $dt->year;
			$month = $dt->month;
			$day = $dt->day;
			$hour = $dt->hour;
			$timestamp = $dt->timestamp;
			$path = "/assets/uploads";
			if (!file_exists(public_path() . $path)) {
				File::makeDirectory(public_path() . $path);
			}
			$mimeType = $file->getClientMimeType();
			$mime = explode('/', $mimeType);
			$name_path = $timestamp . "-" . $year . $month . $day . '-' . (time()) . '-' . (str_slug($file->getClientOriginalName())) . "." . $mime[1];
			$file->move(public_path() . $path, $name_path);
			$image_json = $path . "/" . $name_path;
		}
		return $image_json;
	}
}

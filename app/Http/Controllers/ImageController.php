<?php

namespace App\Http\Controllers;

use App\Entities\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function approve_image(Request $request) {
    	$user_id = $request->get('user_id');
    	$val_approve = $request->get('val_approve');
    	$images = Image::where('user_id', $user_id)->get();

    	if(empty($images)) {
    		return 0;
    	}
        foreach ($images as $key => $img) {
        	$img->approve = $val_approve;
        	$img->save();
        }
		return 1;
    }
}

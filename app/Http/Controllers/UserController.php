<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Entities\Role;
use App\Entities\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository) {
    	$this->repository = $repository;
        $this->middleware('web', ['except' => ['logout']]);
    }

    public function add_user() {
        $categories = Category::all();
        $roles = Role::all();
        return view('admin.user.add', compact('categories', 'roles'));
    }

    public function store(Request $request) {
        if($request->has('role') && ($request->get('role') != 0)) {
            $user = new User;
            $user->email = $request->get('email');
            $user->password = Hash::make($request->get('password'));
            $user->isRegister = 1;
            $user->isSubmit = 1;
            $user->save();
            if($request->get('role') == 2) {
                if($request->get('category') == 0) {
                    return redirect()->back()->withErrors(['code' => 'danger', 'message' => 'Không thêm được']);
                } 
                $user->categories()->attach($request->get('category'));
            } 
            $user->attachRole(Role::find($request->get('role')));
            return redirect()->back()->with(['code' => 'success', 'message' => 'Đã thêm người dùng thành công']);
        } else {
            return redirect()->back()->withErrors(['code' => 'danger', 'message' => 'Không thêm được']);
        }
    }

    public function delete_user_admin(Request $request, $id) {
    	$user = User::find($id);
    	if(!empty($user)) {
    		$result = $this->repository->delete_once($id);
    		return redirect()->back()->with(['code' => 'success', 'message' => 'Đã xóa người dùng thành công']);
    	} else {
    		return redirect()->back()->withErrors(['code' => 'danger', 'message' => 'Không xóa được']);
    	}
    }

    public function logout() {
        Auth::logout();
        return redirect('/login');
    }
}

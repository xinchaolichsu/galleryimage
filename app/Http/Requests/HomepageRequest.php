<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomepageRequest extends FormRequest {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'cate-image' => 'required',
			'image_uploaded' => 'required',
		];
	}

	public function messages() {
		return [
			'cate-image.required' => 'Chọn danh mục cho ảnh',
			'image_uploaded.required' => 'Upload ảnh trước khi hoàn tất',
		];
	}
}

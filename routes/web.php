<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

Route::get('/', 'HomepageController@index');
Route::post('/', 'HomepageController@store');

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
	Route::get('/', 'Admin\AdminController@index');
	Route::get('/edit-user-{id}', 'Admin\AdminController@edit');
	Route::post('/edit-user-{id}', 'Admin\AdminController@update');
	Route::get('/delete-user-{id}', 'UserController@delete_user_admin');
	Route::get('/add-user', 'UserController@add_user');
	Route::post('/add-user', 'UserController@store');

	Route::get('/danh-muc', 'Admin\CategoryController@index');

	Route::get('/download/file/{path}', 'Admin\AdminController@download');
	Route::post('/ajax/approve', 'ImageController@approve_image');
	Route::post('/ajax/sendemail-edituser', 'Admin\AdminController@sendemail_ajax');
	Route::post('/edit-user-{id}/change-image-ajax', 'Admin\AdminController@change_image_ajax');
});

Route::get('/logout', 'UserController@logout');

Route::post('/upload-ajax', 'HomepageController@upload_ajax');
Route::post('/napthe-ajax', 'HomepageController@napthe_ajax');
Route::post('/check-email', 'HomepageController@checkemail');

Route::get('/payment', 'PaymentController@send_payment');
Route::get('/payment/callback', 'PaymentController@response_payment');
Auth::routes();
Route::post('/register', 'Auth\RegisterController@register');

Route::get('/home', 'HomeController@index');

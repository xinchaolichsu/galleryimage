<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;

class CreatePremissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name' => 'View Users',
            'slug' => 'view.users',
            'description' => '',
        ]);

        Permission::create([
            'name' => 'Create Users',
            'slug' => 'create.users',
            'description' => '',
        ]);

        Permission::create([
            'name' => 'Update Users',
            'slug' => 'update.users',
            'description' => '',
        ]);

        Permission::create([
            'name' => 'Delete Users',
            'slug' => 'delete.users',
        ]);

        Permission::create([
            'name' => 'Delete Image',
            'slug' => 'delete.image',
            'description' => '',
        ]);

        Permission::create([
            'name' => 'Update Image',
            'slug' => 'update.image',
            'description' => '',
        ]);
    }
}

<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cates = [
            [
                'name' => 'Ảnh cưới',
            ],
            [
                'name' => 'Ảnh sinh nhật',
            ],
            [
                'name' => 'Ảnh kỷ yếu',
            ],
            [
                'name' => 'Ảnh lưu niệm',
            ],
            [
                'name' => 'Ảnh gia đình',
            ],
            [
                'name' => 'Ảnh bạn bè',
            ],
            [
                'name' => 'Khác',
            ],
        ];
        foreach ($cates as $key => $cate) {
            $new_cate   = new Category;
            $new_cate->name = $cate['name'];
            $new_cate->slug = str_slug($cate['name'], '-');
            $new_cate->save();
        }
    }
}

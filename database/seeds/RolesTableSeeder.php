<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;
use NF\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();
        $admin = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'level' => '1',
        ]);

        foreach ($permissions as $permission) {
            $admin->attachPermission($permission);
        }

        $editor = Role::create([
            'name' => 'Editor',
            'slug' => 'editor',
            'level' => '2',
        ]);

        foreach ($permissions as $permission) {
            if ($permission->slug != 'create.users' && $permission->slug != 'delete.users') {
                $editor->attachPermission($permission);
            }
        }
        $checker = Role::create([
            'name' => 'Checker',
            'slug' => 'checker',
            'level' => '3',
        ]);
        foreach ($permissions as $permission) {
            if ($permission->slug != 'create.users' && $permission->slug != 'update.users' && $permission->slug != 'delete.users' && $permission->slug != 'delete.image' && $permission->slug != 'update.image') {
                $checker->attachPermission($permission);
            }
        }
        $user = Role::create([
            'name' => 'User',
            'slug' => 'user',
            'level' => '4',
        ]);
    }
}

<?php

use App\Entities\Image;
use App\Entities\User;
use Illuminate\Database\Seeder;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::find(1);
        $editorRole = Role::find(2);
        $checkerRole = Role::find(3);
        $userRole = Role::find(4);

        $users = [
            'admin' => [
                'email' => 'admin@gmail.com',
                'role' => 'adminRole',
                'category' => '',
            ],
            'editor1' => [
                'email' => 'editor1@gmail.com',
                'role' => 'editorRole',
                'category' => '1',
            ],
            'editor2' => [
                'email' => 'editor2@gmail.com',
                'role' => 'editorRole',
                'category' => '2',
            ],
            'editor3' => [
                'email' => 'editor3@gmail.com',
                'role' => 'editorRole',
                'category' => '3',
            ],
            'editor4' => [
                'email' => 'editor4@gmail.com',
                'role' => 'editorRole',
                'category' => '4',
            ],
            'editor5' => [
                'email' => 'editor5@gmail.com',
                'role' => 'editorRole',
                'category' => '5',
            ],
            'editor6' => [
                'email' => 'editor6@gmail.com',
                'role' => 'editorRole',
                'category' => '6',
            ],
            'editor7' => [
                'email' => 'editor7@gmail.com',
                'role' => 'editorRole',
                'category' => '7',
            ],
            'checker' => [
                'email' => 'checker@gmail.com',
                'role' => 'checkerRole',
                'category' => '',
            ],
            'user' => [
                'email' => 'user@gmail.com',
                'role' => 'userRole',
                'category' => '',
            ],
        ];
        $i = 1;
        foreach ($users as $key => $u) {
            $user = new User;
            $user->email = $u['email'];
            $user->password = Hash::make('secret');
            $user->isRegister = 1;
            $user->account_id = $i;
            $user->save();
            $user->attachRole(${$u['role']});
            if (!empty($u['category'])) {
                $user->categories()->attach((int) $u['category']);
            }
            $image = new Image;
            $image->user_id = $user->id;
            $image->link = '';
            $image->cate_id = rand(1, 7);
            $image->description = 'default';
            $image->approve = 0;
            $image->save();
            $i++;
        }
    }
}

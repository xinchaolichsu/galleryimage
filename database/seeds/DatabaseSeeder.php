<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CreatePremissionSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(UsersTableSeeder::class);
    }
}

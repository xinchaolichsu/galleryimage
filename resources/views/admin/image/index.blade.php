@extends ('admin.admin')

@section ('content')
@if(Auth::check())
<div class="container">
    @if(Auth::check() && Auth::user()->isRole('admin'))
    <div class="row">
        <div class="col-md-2 col-sm-4 col-xs-8 pull-right">
            <a href="{!! url('/admin/add-user') !!}"><button class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Thêm người dùng</button></a>
        </div>
    </div>
    <br/>
    @endif
    <table cellspacing="0" class="table table-striped table-bordered" id="show-image" width="100%">
        <thead>
            <tr>
                <th>
                    Email
                </th>
                <th>
                    Đăng ký
                </th>
                <th>
                    Thanh toán
                </th>
                <th>
                    Phê duyệt
                </th>
                <th>
                    Ngày tải lên
                </th>
                <th>
                    Chức năng
                </th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>
                    Email
                </th>
                <th>
                    Đăng ký
                </th>
                <th>
                    Thanh toán
                </th>
                <th>
                    Phê duyệt
                </th>
                <th>
                    Ngày tải lên
                </th>
                <th>
                    Chức năng
                </th>
            </tr>
        </tfoot>
        <tbody>
            @if (!empty($users))
                @foreach ($users as $user)
            <tr>
                <td>
                    {!! $user->email !!}
                </td>
                <td>
                    {!! $user->isRegister !!}
                </td>
                <td>
                    {!! $user->payment !!}
                </td>
                <td>
                    {!! $user->approve !!}
                </td>
                <td>
                    {{ $user->created_at }}
                </td>
                <td>
                    <a class="btn btn-primary" href="{!! url('/admin/edit-user-'.($user->id)) !!}">
                        Chi tiết
                    </a>
                    @if(Auth::check() && Auth::user()->isRole('admin'))
                    <button class="btn btn-danger" data-target="#delete_modal_{{ $user->id }}" data-toggle="modal" type="button">
                        Xóa
                    </button>
                    <div class="modal fade" id="delete_modal_{{ $user->id }}" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <p>
                                        Bạn chắc chắn muốn xóa người dùng này?
                                    </p>
                                    <a href="{{ url('/admin/delete-user-'.$user->id) }}"><button class="btn btn-danger" id="btn-delete-statistic" stt="{{ $user->id }}" token="{{ csrf_token() }}" type="submit">
                                        Xóa
                                    </button></a>
                                    <input name="stt" type="hidden" value="{{ $user->id }}">
                                    </input>
                                    <input id="csrf-token" name="_token" type="hidden" value="{{ csrf_token() }}">
                                    </input>
                                    <button class="btn btn-default" data-dismiss="modal" type="button">
                                        Hủy
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#show-image').DataTable({
            responsive: true
        });
    });
</script>
<br/><br/>
@endif
@stop

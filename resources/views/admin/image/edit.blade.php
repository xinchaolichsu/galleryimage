@extends ('admin.admin')

@section ('content')
@if(Auth::id() == $user->id || Auth::user()->isRole('admin|editor|checker'))
<div class="container" id="image-edit-page">
    <div class="row btn-back">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <a href="{!! url('/admin') !!}"><button class="btn btn-info"><i class="fa fa-backward" aria-hidden="true"></i>Trở lại</button></a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            @include('blocks.notify_code')
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="@if(Auth::user()->isRole('user')) {{'col-md-12'}} @else {{'col-md-6'}}  @endif col-sm-12 col-xs-12">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Thông tin:
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="single_email">
                                    Email:
                                </label>
                                <input class="form-control" disabled="" id="single_email" name="email" type="email" value="{{ $user->email }}">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="single_email">
                                    Ngày tạo:
                                </label>
                                <input class="form-control" disabled="" name="email" type="email" value="{{ $user->created_at }}">
                                </input>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lịch sử thanh toán:
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="alert alert-danger">Chưa có !</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Lời nhắn:
                        </div>
                        <div class="panel-body">
                            @if(!empty($user->image))
                                @foreach($user->image as $key_img => $img)
                                    <div class="form-group">
                                        + {{ $img['description'] }} - {{ $img['created_at'] }}
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::user()->isRole('admin|editor|checker'))
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Chế độ duyệt
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>
                                    <input class="mode-approve" name="approve[]" type="radio" value="1" @if($user->approve == 1) checked="" @endif>
                                        Đã duyệt
                                    </input>
                                </label>
                                <br/>
                                <label>
                                    <input class="mode-approve" name="approve[]" type="radio" value="2" @if($user->approve == 2) checked="" @endif>
                                        Không duyệt
                                    </input>
                                </label>
                            </div>
                            <p class="fa fa-spinner fa-spin garung-spinner hide" style="font-size:24px"></p>
                            <div id="result-approve"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Gửi e-mail đến người dùng
                        </div>
                        <div class="panel-body">
                            <div id="result-sendemail-edituser"></div>
                            <div class="form-group">
                                <label for="single_email">
                                    Email:
                                </label>
                                <input class="form-control" disabled="" id="single_email" type="email" value="{{ $user->email }}">
                                </input>
                            </div>
                            <div class="form-group">
                                <label for="content-sendemail">
                                    Nội dung:
                                </label>
                                <textarea class="form-control" id="content-sendemail" rows="3"></textarea>
                            </div>
                            <div class="">
                                <button class="btn btn-primary" id="btn-sendemail-edituser" type="button">Gửi</button>
                                <p class="fa fa-spinner fa-spin garung-spinner-email hide" style="font-size:24px"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div class="row container-images">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="result-change-image"></div>
        </div>
        <form method="post" role="form">
        @if(!empty($user->image))
            @foreach($user->image as $key_image => $img)
                @if(!empty($img['link']))
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group category-container">
                            <label for="single_email" >
                                <div class="thumbnail">
                                    <div class="caption">
                                        Chuyên mục: <h4>{{ $img['cate_name'] }}</h4>
                                    </div>
                                </div>
                            </label>
                        </div>
                        @foreach ($img['link'] as $key => $link)
                        <div class="wrapp-image-{{ $key }} list-image-container">
                            <div class="col-xs-6 col-sm-4 col-md-3">
                                <div class="thumbnail">
                                    <div class="caption">
                                        {{-- <h4>Ảnh số {{ $key }}</h4> --}}
                                        {{-- <p>{!! 'Không có mô tả' !!}</p> --}}
                                        @if(Auth::user()->isRole('admin|editor'))
                                        <p>
                                            <a href="{!! url('/admin/download/file/'.$link['download']) !!}" class="label label-default">Download</a>
                                            <a href="#" class="label label-primary" data-toggle="modal" data-target="#upload_modal_{{ $key }}">Upload</a>
                                            <a type="button" class="label label-danger delete_btn_edit" data-parent="wrapp-image-{{ $key }}" data-val-image="val-image-{{ $key }}">Xóa</a>
                                        </p>
                                        @endif
                                    </div>
                                    <input type="hidden" name="link[]" value="{!! $link['url'] !!}" class="val-image-{{ $key }}">
                                    {{ csrf_field() }}
                                    <img src="{!! $link['url'] !!}" alt="..." class="area_inp_file_{{ $key }}">
                                </div>
                            </div>
                        </div>
                        <div id="upload_modal_{{ $key }}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Upload ảnh tại đây để thay thế ảnh cũ</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group" role="group">
                                            <input type="file" class="form-control " name="file-change" id="inp_file_{{ $key }}" />
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="container-loading pull-left">
                                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw spinner-loading"></i>
                                            <span class="message">Vui lòng chờ trong giây lát ...</span>
                                        </span>
                                        <button type="button" class="btn btn-default submit-change-image" data-submit="submit-change-image" data-token="{{ csrf_token() }}" data-input-image="inp_file_{{ $key }}" data-input-save-image="val-image-{{ $key }}">Upload</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                                        <span class="pull-left result-change-image-2"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <hr>
                @endif
            @endforeach
            <div class="col-xs-12 btn-update">
                <button type="submit" class="btn btn-primary">
                    Cập nhật
                </button>
            </div>
            <script type="text/javascript">
            $(document).ready(function(){
                //=============== for appove image ===============
                $('.mode-approve').change(function(){
                    var val_approve = $(this).val();
                    var str = '';
                    $.ajax({
                        url: URL + '/admin/ajax/approve',
                        data: {
                            user_id: {{ $user->id }},
                            val_approve: val_approve
                        },
                        type: 'POST',
                        beforeSend: function () {
                            $('.garung-spinner').removeClass('hide');
                            $('#result-approve').addClass('hide');
                        },
                        success: function(response) {
                            console.log(response);
                            $('.garung-spinner').addClass('hide');
                            if(response == 1) {
                                str = '<div class="alert alert-success">Lưu thành công</div>';
                                $('#result-approve').html(str);
                            } else {
                                str = '<div class="alert alert-danger">Lưu không thành công</div>';
                                $('#result-approve').html(str);
                            }
                            $('#result-approve').removeClass('hide');
                        },
                        error: function($error) {
                            console.log($error);
                            $('.garung-spinner').addClass('hide');
                            $('#result-approve').removeClass('hide');
                            str = '<div class="alert alert-danger">Lưu không thành công</div>';
                            $('#result-approve').html(str);
                        }
                    });
                });
            });
            </script>
        @else
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="alert alert-danger">Không có ảnh nào được tải lên hoặc đã bị xóa bỏ.</div>
                </div>
            </div>
        @endif
        </form>
    </div>
</div>
<br/><br/><br/>
<script type="text/javascript">
    $(document).ready(function(){
        //=============== for send mail ==================
        $('#btn-sendemail-edituser').click(function(){
            var content_sendemail = $('#content-sendemail').val();
            var str = '';
            $.ajax({
                url: URL + '/admin/ajax/sendemail-edituser',
                data: {
                    email: "{{ $user->email }}",
                    content: content_sendemail
                },
                type: 'POST',
                beforeSend: function () {
                    $('.garung-spinner-email').removeClass('hide');
                    $('#result-sendemail-edituser').addClass('hide');
                },
                success: function(response) {
                    console.log(response);
                    $('.garung-spinner-email').addClass('hide');
                    if(response == 1) {
                        str = '<div class="alert alert-success">Gửi thành công</div>';
                        $('#result-sendemail-edituser').html(str);
                    } else {
                        str = '<div class="alert alert-danger">Gửi E-mail không thành công</div>';
                        $('#result-sendemail-edituser').html(str);
                    }
                    $('#result-sendemail-edituser').removeClass('hide');
                },
                error: function($error) {
                    console.log($error);
                    $('.garung-spinner-email').addClass('hide');
                    $('#result-sendemail-edituser').removeClass('hide');
                    str = '<div class="alert alert-danger">Gửi E-mail không thành công</div>';
                    $('#result-sendemail-edituser').html(str);
                }
            });
        });
        //=============== for delete image ===============
        $('.delete_btn_edit').click(function() {
            if (confirm("Bạn chắc chắn muốn xóa ảnh này?")) {
                var data_parent = $(this).attr('data-parent');
                var data_val_image = $(this).attr('data-val-image');
                $('.'+data_val_image).val('');
                $('.'+data_parent).css('display', 'none');
            }
        });
        //=============== for upload to change image =====
        $('.container-loading').addClass('hide');
        $('.result-change-image').addClass('hide');
        $('.submit-change-image').click(function() {
            $('.result-change-image').html('');
            $('.result-change-image-2').html('');
            var _token = $(this).attr('data-token');
            var _image = $(this).attr('data-input-image');
            var _inp_image_to_save = $(this).attr('data-input-save-image');
            var file_data = '';

            var form_data = new FormData();
            file_data = $('#' + _image).get(0).files[0];
            form_data.append("file", file_data);
            form_data.append("_token", _token);
            //============================================================
            $.ajax({
                url: URL + '/admin/edit-user-{{ $user->id }}/change-image-ajax',
                data: form_data,
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('.container-loading').removeClass('hide');
                },
                success: function (data) {
                    $('.result-change-image').html('');
                    $('.result-change-image-2').html('');
                    var i;
                    var str = '';
                    $('.container-loading').addClass('hide');
                    if (data !== '') {
                        $('.area_' + _image).attr('src', '' + data);
                        $('.' + _inp_image_to_save).val(data);
                        str = '<div class="alert alert-success" style="margin-bottom: 5px;">Upload thành công</div>';
                        $('.result-change-image').html(str);
                        $('.result-change-image-2').html(str);
                        $('.submit-change-image').addClass('hide');
                    } else {
                        str = '<div class="alert alert-danger">Upload không thành công</div>';
                        $('.result-change-image').html(str);
                        $('.result-change-image-2').html(str);
                    }
                    $('.result-change-image').removeClass('hide');
                }
            });
        });
    });
</script>
@endif
@stop

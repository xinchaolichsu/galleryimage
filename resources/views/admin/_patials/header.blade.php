<div class="container-fluid">
    <div class="admin_home">
        <div class="admin_home_container">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button aria-expanded="false" class="navbar-toggle collapsed" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" type="button">
                            <span class="sr-only">
                                Toggle navigation
                            </span>
                            <span class="icon-bar">
                            </span>
                            <span class="icon-bar">
                            </span>
                            <span class="icon-bar">
                            </span>
                        </button>
                        <a class="navbar-brand" href="{!! url('/') !!}">
                            <i class="fa fa-home">
                            </i>
                            Home
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="{!! url('/admin') !!}">
                                    <i aria-hidden="true" class="fa fa-user">
                                    </i>
                                    Dashboard
                                </a>
                            </li>
                            @if(Auth::user()->isRole('admin'))
                            <li class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                                    Quản lý
                                    <span class="caret">
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{!! url('/admin') !!}">
                                            <i aria-hidden="true" class="fa fa-user">
                                            </i>
                                            Quản trị người dùng
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                                    <i aria-hidden="true" class="fa fa-user-circle">
                                    </i>
                                    {{ Auth::user()->name }}
                                    <span class="caret">
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    {{-- <li class="divider" role="separator">
                                    </li> --}}
                                    <li>
                                        <a href="{!! url('/logout') !!}">
                                            <i aria-hidden="true" class="fa fa-times">
                                            </i>
                                            Đăng xuất
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
</div>
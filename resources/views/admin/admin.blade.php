<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                home
            </title>
            <link href="{!! asset('assets/css/library.min.css') !!}" rel="stylesheet">
            </link>
            <link href="{!! asset('assets/css/backend.css') !!}" rel="stylesheet">
            <script src="{!! asset('assets/js/library.js') !!}">
            </script>
            <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
            </link>
            <script type="text/javascript">
                var URL = "{{ url('/') }}";
            </script>
        </meta>
    </head>
    <body>
        @include('admin._patials.header')
        <div class="content_admin">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        @include('blocks.notify_code')
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
        <script src="{!! asset('assets/js/backend.js') !!}">
        </script>
    </body>
</html>

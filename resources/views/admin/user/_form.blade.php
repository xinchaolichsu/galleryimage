<form method="post" role="form">
    <div class="panel panel-default">
        <div class="panel-heading">
            Thêm người dùng mới:
        </div>
        <div class="panel-body">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="single_email">
                        Email đăng nhập:
                    </label>
                    <input class="form-control" required="" id="single_email" name="email" type="email">
                    </input>
                </div>
                <div class="form-group">
                    <label for="single_email">
                        Password:
                    </label>
                    <input class="form-control" required="" name="password" type="password">
                    </input>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="">
                        Chức vụ:
                    </label>
                    <select class="form-control" name="role" id="choice_role_adduser">
                    	<option value="0">Chọn chức vụ</option>
                    	@if($roles)
	                    	@foreach ($roles as $role)
	                    		<option value="{{ $role->id }}">{{ $role->name }}</option>
	                    	@endforeach
                    	@endif
                    </select>
                </div>
                <div class="form-group"  id="choice_cate_adduser">
                    <label for="">
                        Chuyên mục:
                    </label>
                    <select class="form-control" name="category">
                    	<option value="0">Chọn chuyên mục cho Editor</option>
                    	@if($categories)
	                    	@foreach ($categories as $category)
	                    		<option value="{{ $category->id }}">{{ $category->name }}</option>
	                    	@endforeach
                    	@endif
                    </select>
                </div>
            </div>
            <div class="col-md-3 col-sm-8 col-xs-8 col-md-push-1 area-btn-submit">
            	<div class="form-group">
            		<input id="csrf-token" name="_token" type="hidden" value="{{ csrf_token() }}"></input>
            		<button type="submit" class="btn btn-primary btn-submit-add" >
            			<i class="fa fa-plus" aria-hidden="true"></i> Thêm người dùng
            		</button>
            	</div>
            </div>
        </div>
    </div>
</form>
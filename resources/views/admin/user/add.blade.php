@extends ('admin.admin')

@section ('content')
	<div class="container" id="add_user_page">
		<div class="row">
		@include('admin.user._form')
		</div>
	</div>
@stop
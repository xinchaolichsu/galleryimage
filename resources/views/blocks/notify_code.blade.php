@if (Session::has('code'))
<div class="alert alert-{!! Session::get('code') !!}">
    {!! Session::get('message') !!}
</div>
@endif

<div class="container-fluid" id="contain-page">
    <nav class="navbar navbar-inverse header">
        <div class="navbar-header">
            <button class="navbar-toggle" data-target=".js-navbar-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">
                    Toggle navigation
                </span>
                <span class="icon-bar">
                </span>
                <span class="icon-bar">
                </span>
                <span class="icon-bar">
                </span>
            </button>
        </div>
        <div class="collapse navbar-collapse js-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="title-menu">
                    <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                        Dịch vụ
                    </a>
                </li>
                <li class="title-menu">
                    <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                        Sản phẩm
                    </a>
                </li>
                <li class="title-menu">
                    <a aria-expanded="false" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button">
                        Mới
                    </a>
                </li>
                @if(Auth::check())
                    <li class="title-menu">
                        <a  href="{!! url('/admin') !!}" >
                            Quản lý thông tin
                        </a>
                    </li>
                    <li class="title-menu">
                        <a  href="{!! url('/logout') !!}" >
                            Đăng xuất
                        </a>
                    </li>
                @else
                <li class="title-menu">
                    <a href="{!! url('/login') !!}" >
                        Đăng nhập
                    </a>
                </li>
                @endif
            </ul>
        </div>
        <!-- /.nav-collapse -->
    </nav>
</div>
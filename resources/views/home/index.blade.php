@extends ('frontend')

@section ('content')
<br/><br/>
<div class="contain-main">
	<div class="row">
		<div class="col-md-7 col-md-push-3 col-sm-9 col-xs-12">
			@include('blocks.notify_code')
		</div>
	</div>
    <form enctype="multipart/form-data" id="contain-form" method="post" role="form">
    	<div class="row form-group">
    		<div class="col-md-7 col-md-push-3 col-sm-9 col-xs-12">
    			<div class="thongbao"></div>
    		</div>
    		<div class="col-md-7 col-md-push-3 col-sm-9 col-xs-12">
            	<div class="col-md-6 col-sm-12 col-xs-12">
            		@if(!empty(Auth::user()))
            			<label>Email của bạn là: </label>
            			<p style="color: red">{{ Auth::user()->email }}</p>
            		@else
	            		<label>Email (<span style="color: red">*</span>) - Nhập email của bạn trước tiên: </label>
	            		<input type="email" class="form-control" name="email" required="" id="inp-email">
	            		</input>
	            		<br>
	            		<p class="fa fa-spinner fa-spin garung-spinner-email hide" style="font-size:24px"></p>
	            		<div id="result-sendemail-edituser"></div>
	            	@endif
            	</div>
            	<div class="col-md-6 col-sm-12 col-xs-12">
	            	<label>Danh mục cho ảnh (<span style="color: red">*</span>):</label>
	            	<select class="cate-image form-control" name="cate-image" required="">
	            		<option value="0">Chọn danh mục</option>
	            		@if(!empty($categories))
		            		@foreach($categories as $cate)
		            			<option value="{{ $cate->id }}">{{ $cate->name }}</option>
		            		@endforeach
	            		@endif
	            	</select>
            	</div>
            </div>
    	</div>
        <div class="row form-group">
            <div class="col-md-7 col-md-push-3 col-sm-9 col-xs-12">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="fileUpload btn">
                        <i class="fa fa-camera-retro fa-4x">
                        </i>
                        <input type="hidden" id="form-data-u" name="form-data-u" value="">
                        <input type="hidden" id="image_uploaded" name="image_uploaded" value=""></input>
                        <input type="hidden" id="check_image" name="check_image" value="" ></input>
                        <input data-token="{{ csrf_token() }}" id="inp-upload" class="upload" multiple="" type="file" name="image[]"/>
                        upload
                    </div>
                    <br>
                    <button type="button" class="btn btn-primary upload-ajax" id="upload-ajax">Tải lên</button>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="click-requirement">
                        <button class="btn" data-target="#garung-requirement" data-toggle="modal" type="button">
                            <i aria-hidden="true" class="fa fa-pencil fa-4x">
                            </i>
                            Yêu cầu
                        </button>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                	<div class="click-payment">
	                	<button class="btn btn-background" type="button" id="btn-payment">
		                    <i aria-hidden="true" class="fa fa-credit-card fa-4x">
		                    </i>
		                    Thanh toán
		                </button>
		                {{-- <button class="btn btn-primary" id="btn-payment" type="button">
		                    Thanh toán qua thẻ nạp
		                </button>
		                <br/><br/>
		                <a href="{{ $btn_url }}"><img src="http://www.baokim.vn/developers/uploads/baokim_btn/thanhtoan-l.png" alt="Thanh toán an toàn với Bảo Kim !" border="0" title="Thanh toán trực tuyến an toàn dùng tài khoản Ngân hàng (VietcomBank, TechcomBank, Đông Á, VietinBank, Quân Đội, VIB, SHB,... và thẻ Quốc tế (Visa, Master Card...) qua Cổng thanh toán trực tuyến BảoKim.vn" ></a> --}}
		            </div>
                </div>
            </div>
        </div>
	    <div class="modal fade" id="garung-requirement" role="dialog">
	        <div class="modal-dialog">
	            <div class="modal-content">
	                <div class="modal-body">
	                    <p>
	                        Gửi yêu cầu của bạn đến quản trị viên:
	                    </p>
	                    <textarea class="form-control" name="requirement"></textarea>
	                    <br/>
	                    <button class="btn btn-default" data-dismiss="modal" type="button">
	                        Xong
	                    </button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <div class="content-main row">
	    	<div class="container-payment col-md-7 col-md-push-3 col-sm-9 col-xs-12">
	            <div class="panel with-nav-tabs panel-default">
	                <div class="panel-heading">
	                    <ul class="nav nav-tabs">
	                        <li class="active">
	                            <a data-toggle="tab" href="#tab1default">
	                                Thanh toán qua Visa
	                            </a>
	                        </li>
	                        <li>
	                            <a data-toggle="tab" href="#tab2default">
	                                Thanh toán qua thẻ điện thoại
	                            </a>
	                        </li>
	                    </ul>
	                </div>
	                <div class="panel-body">
	                    <div class="tab-content">
	                        <div class="tab-pane fade in active" id="tab1default">
	                            <div class="col-md-12 col-sm-12 col-xs-12">
	                                   	<a href="{{ $btn_url }}"><img src="http://www.baokim.vn/developers/uploads/baokim_btn/thanhtoan-l.png" alt="Thanh toán an toàn với Bảo Kim !" border="0" title="Thanh toán trực tuyến an toàn dùng tài khoản Ngân hàng (VietcomBank, TechcomBank, Đông Á, VietinBank, Quân Đội, VIB, SHB,... và thẻ Quốc tế (Visa, Master Card...) qua Cổng thanh toán trực tuyến BảoKim.vn" ></a>
	                            </div>
	                        </div>
	                        <div class="tab-pane fade" id="tab2default">
	                            <div class="form-row">
	                                <div class="col-md-4 col-sm-4 col-xs-12 form-group required">
	                                    <label class="control-label">
	                                        Loại thẻ:
	                                    </label>
	                                    <select class="form-control" id="card_id">
	                                        <option value="VIETTEL">
	                                            Viettel
	                                        </option>
	                                        <option value="VINA">
	                                            Vinaphone
	                                        </option>
	                                        <option value="MOBI">
	                                            Mobifone
	                                        </option>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="form-row">
	                                <div class="col-md-4 col-sm-4 col-xs-12 form-group required">
	                                    <label class="control-label">
	                                        Mã Pin của thẻ:
	                                    </label>
	                                    <input class="form-control" id="pin_field" size="4" type="text">
	                                    </input>
	                                </div>
	                            </div>
	                            <div class="form-row">
	                                <div class="col-md-4 col-sm-4 col-xs-12 form-group card required">
	                                    <label class="control-label">
	                                        Số seri thẻ:
	                                    </label>
	                                    <input autocomplete="off" class="form-control card-number" id="seri_field" size="20" type="text">
	                                    </input>
	                                </div>
	                            </div>
	                            <div class="form-row">
	                                <div class="col-md-4 col-sm-4 col-xs-12 form-group">
	                                    <button class="form-control btn btn-primary submit-button standard-button" id="pay-napthe" type="button">
	                                        Thanh toán »
	                                    </button>
	                                </div>
	                            </div>

				                <input type="hidden" name="result_napthe" value="" id="result_napthe">
	                        </div>
	                    </div>
	                </div>
	                <div class="panel-footer">
	                	<button type="button" class="btn btn-default close-payment">
                        	Đóng
                        </button>
	                </div>
	            </div>
	        </div>
	    	<div class="upload-result col-md-7 col-md-push-3 col-sm-9 col-xs-12">
		    </div>
		</div>
	    <div class="container-complete">
		    <div class="row">
		    	<div class="col-md-3 col-md-push-5 col-sm-9 col-xs-12">
		    		<input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
		            <input type="submit" class="btn btn-primary btn-complete" id="btn-complete" value="Hoàn tất">
		            </input>
	            </div>
		    </div>
        </div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		var val_inp_email = $('#inp-email').val();
		if(val_inp_email == '') {
			$('.btn-complete').attr('disabled', 'true');
		}

		$('#inp-email').on("keyup", function() {
			send_ajax_check_email($(this).val());
		});

		$('#inp-email').on("keypress", function() {
			send_ajax_check_email($(this).val());
		});

		$('#inp-email').on("focus", function() {
			send_ajax_check_email($(this).val());
		});

		$('#inp-email').on("blur", function() {
			send_ajax_check_email($(this).val());
		});

		function send_ajax_check_email (email) {
			$.ajax({
				url: URL + '/check-email',
				type: 'POST',
				data: {
					email: email
				},
				beforeSend: function () {
                    $('.garung-spinner-email').removeClass('hide');
                    $('#result-sendemail-edituser').addClass('hide');
                },
                success: function(response) {
                    console.log(response);
                    var result = jQuery.parseJSON(response);
                    $('.garung-spinner-email').addClass('hide');
                    if(result.code == 2) {
                        str = '<div style="color: blue;">'+ result.message +'</div>';
                    	$('.btn-complete').removeAttr('disabled');
                    }else {
                        str = '<div style="color: red;">'+ result.message +'</div>';
                    }
                    $('#result-sendemail-edituser').html(str);
                    $('#result-sendemail-edituser').removeClass('hide');
                },
                error: function($error) {
                    console.log($error);
                    $('.garung-spinner-email').addClass('hide');
                    $('#result-sendemail-edituser').removeClass('hide');
                    str = '<div class="alert alert-danger">Gửi E-mail không thành công</div>';
                    $('#result-sendemail-edituser').html(str);
                }
			});
		}
	});
</script>
@stop

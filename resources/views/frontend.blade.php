<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>
                Feature Image
            </title>
            <!-- Latest compiled and minified CSS -->
            <link href="{!! asset('assets/css/library.min.css') !!}" rel="stylesheet">
            </link>
            <link href="{!! asset('assets/css/frontend.css') !!}" rel="stylesheet">
            </link>
            <script src="{!! asset('assets/js/library.js') !!}">
            </script>
            <script type="text/javascript">
                var URL = "{{ url('/') }}";
            </script>
        </meta>
    </head>
    <body>
        @include('_patials.header')
        <div class="container" id="contain-page">
            <div class="logo">
                <h1>
                    Logo
                </h1>
            </div>
            @yield ('content')
            <br /><br />
        </div>
        <script src="{!! asset('assets/js/frontend.js') !!}">
        </script>
    </body>
</html>

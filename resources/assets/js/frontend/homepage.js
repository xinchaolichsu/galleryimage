$(document).ready(function() {
    $('.container-payment').css('display', 'none');
    $('#btn-payment').click(function() {
        $('.container-payment').css('display', 'block');
        // if (this.id == 'btn-payment') {
        // } else {
        //     $('.container-payment').css('display', 'none');
        // }
    });
    $('.close-payment').click(function() {
        $('.container-payment').css('display', 'none');
    });
    $('#upload-ajax').click(function() {
        var _token = $('.upload').attr('data-token');
        var images = [];
        var file_data2 = $(".upload").prop("files");   
        console.log(file_data2);
        var form_data = new FormData();                  
        for (var i = 0; i < $('#inp-upload').get(0).files.length; ++i) {
            var file_data = $('#inp-upload').get(0).files[i];
            form_data.append("file[]", file_data);
        }
        //============================================================
        $.ajaxSetup({
            headers: {
                'X-XSRF-TOKEN': _token
            }
        });
        $.ajax({
            url: URL + '/upload-ajax',
            data: form_data,                         
            type: 'post',
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                $('.upload-result').html('');
                var i;
                var str = '';
                if (data !== '') {
                    $('#image_uploaded').val(data);
                    var response = $.parseJSON(data);
                    $('#image_uploaded').attr('check', response.image_id);
                    var url_image = response.url_image;

                    console.log(url_image);
                    for (i = 0 ; i < url_image.length; i++) {
                        str = '';
                        str += '<div class="col-md-2 col-sm-4 col-xs-6">';
                        str += '<div class="form-control single-image" style="height: 100px; width: 100px; position: relative; padding: 3px !important;">';
                        str += '<img style="width:100%; height:100%; border-radius: 5px;" alt="'+ url_image[i] +'" src="'+ URL + url_image[i] +'"/>';
                        str += '</div>';
                        str += '</div>';
                        $('.upload-result').append(str);
                    }
                    $('#form-data-u').val(response.user_id);
                    $('#check_image').val(response.image_id);
                } else {
                    str = '';
                    str += '<div class="col-md-6 col-sm-12 col-xs-12">';
                    str += '<div class="alert alert-danger">';
                    str += '<h3>Chọn ảnh upload trước !</h3>';
                    str += '</div>';
                    str += '</div>';
                    $('.upload-result').append(str);
                }
            }
        });
    });
    $('#pay-napthe').click(function() {
        var card_id = $('#card_id').val();
        var seri_field = $('#seri_field').val();
        var pin_field = $('#pin_field').val();
        $.ajax({
            url: URL + "/napthe-ajax",
            type: 'POST',
            data: {
                seri_field: seri_field,
                pin_field: pin_field,
                card_id: card_id,
            },
            success: function (data) {
                if(data === false || data === 'false') {
                    var thongbao_false = '<div class="alert alert-danger">Thẻ nạp không đúng hoặc không hợp lệ ! Thử lại.</div>';
                    $('.thongbao').append(thongbao_false);
                }
                var result = $.parseJSON(data);
                // var result = $.parseJSON(result1.message);
                console.log(result);
                if((result.status === 200) || (result.status === '200')) {
                    $('#result_napthe').val(result.transaction_id);
                    var thongbao = '<div class="alert alert-success">Thanh toán thành công với thẻ cào mệnh giá ' + result.amount + '</div>';
                    $('.thongbao').append(thongbao);
                    console.log(result.transaction_id);
                } else {
                    var thongbao_error = '<div class="alert alert-danger">' + result.message + '</div>';
                    $('.thongbao').append(thongbao_error);
                    console.log(result.transaction_id);
                }
            }
        });
    });
});
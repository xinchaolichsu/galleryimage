var gulp = require('gulp');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = function() {
    gulp.src([
    		'./bower_components/datatables.net/js/jquery.dataTables.js',
            './resources/assets/js/libraries/*.js',
            './resources/assets/js/backend/*.js',
        ])
        .pipe(concat("backend.js"))
        .pipe(gulp.dest('public/assets/js/'))
}

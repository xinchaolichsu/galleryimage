module.exports = [
    [
        'sass',
        'sass_backend',
        'sass_frontend',
        'minify_css',
        'minify_backend_css',
        'jshint',
        'minify_js',
        'minify_backend_js',
        'minify_frontend_js'
    ]
];

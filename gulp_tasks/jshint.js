var gulp = require('gulp');
var jshint = require('gulp-jshint');
var fixmyjs = require("gulp-fixmyjs");
module.exports = function() {
    return gulp.src([
            './resources/assets/js/**/*.js',
            './resources/assets/angular/**/*.js'
        ])
    	.pipe(fixmyjs(jshint()))
        // .pipe(jshint())
        .pipe(jshint.reporter('default'));
}

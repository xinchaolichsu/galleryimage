var gulp = require('gulp');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = function() {
    gulp.src([
            './resources/assets/js/frontend/*.js',
        ])
        .pipe(concat("frontend.js"))
        .pipe(gulp.dest('public/assets/js/'))
}

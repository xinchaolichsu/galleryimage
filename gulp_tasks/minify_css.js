var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = function() {
    return gulp.src([
    		'./bower_components/bootstrap/dist/css/bootstrap.min.css',
    		'./bower_components/font-awesome/css/font-awesome.css',
            // '/resources/assets/js/libraries/dataTables.bootstrap.min.css'
        ])
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(concat("library.min.css"))
        .pipe(gulp.dest('public/assets/css'));
};

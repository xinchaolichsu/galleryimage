var gulp = require('gulp');
var watch = require('gulp-watch');
module.exports = function() {
    watch('./resources/assets/js/**/*.js', function() {
        gulp.run([
            'jshint',
            'minify_js'
        ]);
    });

    // gulp.watch('./resources/assets/sass/frontend/**/*.scss', ['sass']);
    gulp.watch('./resources/assets/sass/backend/**/*.scss', ['sass_backend']);
    gulp.watch('./resources/assets/sass/frontend/**/*.scss', ['sass_frontend']);
}

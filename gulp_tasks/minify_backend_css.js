var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = function() {
    return gulp.src([
    		'./bower_components/datatables.net-dt/css/jquery.dataTables.min.css',
            './resources/assets/css/library_extension/*.css',
            './resources/assets/css/*.css'
        ])
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(concat("package.min.css"))
        .pipe(gulp.dest('public/assets/css'));
};

var gulp = require('gulp');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = function() {
    gulp.src([
    		'./bower_components/jquery/dist/jquery.min.js',
    		'./bower_components/bootstrap/dist/js/bootstrap.min.js',
    		'./bower_components/jquery-ui/jquery-ui.min.js'
        ])
        .pipe(concat("library.js"))
        .pipe(gulp.dest('public/assets/js/'))
}
